// dependencies
const path = require("path");
const fs = require("fs");
const https = require("https");
const express = require("express");
const socketIO = require("socket.io");

// set path to client side files
const publicPath = path.join(__dirname, "/../public");

// set keys for local development
// directions on how to get these onto your local machine are in the README
/* adapted from: https://stackoverflow.com/questions/11744975/enabling-https-on-express-js/11745114#11745114 */
/* adapted from: https://nodejs.org/en/knowledge/HTTP/servers/how-to-create-a-HTTPS-server/ */
var privateKey = fs.readFileSync('key.pem', 'utf8');
var certificate = fs.readFileSync('cert.pem', 'utf8');
var credentials = { key: privateKey, cert: certificate };
/* end citation */

// lets use espress to make things easier
let app = express();

// serve files from pubic dir set above
app.use(express.static(publicPath));

// start server
/* adapted from: https://stackoverflow.com/questions/11744975/enabling-https-on-express-js/11745114#11745114 */
let httpsServer = https.createServer(credentials, app);
// allow our server to access the socket.io lib
let io = socketIO(httpsServer);
// listen on https port
httpsServer.listen(8443);
/* end citation */

// when someone connects, log it to the console using a callback function
io.on('connection', (socket) => {
  console.log('new user connected');

  // when someone disconnects, log it to the console using a callback function
  // note we use the local referece "socket" and not the global "io"
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

  // event listener for playerDataSend function in client.js
  // we'll use an es6 arrow callback function, as we do other places,
  // because that's what the kids are doing these days I guess
  // note we use the local referece "socket" and not the global "io"
  socket.on('phoneDataSend', (e) => {
    console.log(e);

    // If you want to send this data back to all other connected devices,
    // you need to broadcast it from the server.
    // Here you can use "socket.broadcast.emit" to send to everyone
    // execpt the person who sent the data;
    // or your can use global "io.broadcast.emit" to send the data to everyone
    // including the original sender.

    // everyone except sender
    // socket.broadcast.emit('phoneDataSend', e);

    // everyone including sender
    // io.broadcast.emit('phoneDataSend', e);

  });
})
