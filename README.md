<h1>Sending and Receiving Device Data with P5js, NodeJS, Express, and sockets.io</h1>

<p>Theres a number of steps you have to take to download this and have it run locally. With limits in place from iOS on accessing decive data and the requirement for https, developing with localhost is tricky, here's how I got it to work.</p>

<p>PS. I am using a Mac for this, so the terminal commands might be different for you. I'll try to come back and add them at some point.</p>

<p>Launch Terminal and follow these steps...(don't copy the $ in the code snippets, thats just to indicate its a terminal command)</p>

Install Homebrew (if you don't already have it)<br>
<code>$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"</code>

Install NodeJS<br>
<code>$ brew install node</code>

Install GIT<br>
<code>$ brew install git</code>

CD to the desktop (or wherever you want to keep the app files)<br>
<code>$ cd ~/Desktop</code>

Clone this Repo<br>
<code>$ git clone https://gitlab.com/satoshiswife/socket_test</code>

CD into the app directory (i should probably rename this folder, but whatever)<br>
<code>$ cd socket_test</code>

Install the NPM Packages<br>
<code>$ npm install</code>

Update the NPM Packages<br>
<code>$ npm update</code>

<p>Ok, here is where you have to generate self-signed SSL Certificates. This will allow you to use https, which is required by iOS to send device data -- whether local or remote.<br>
(These commands and instructions are from https://nodejs.org/en/knowledge/HTTP/servers/how-to-create-a-HTTPS-server/)</p>

<code>$ openssl genrsa -out key.pem</code>

<code>$ openssl req -new -key key.pem -out csr.pem</code>

<code>$ openssl x509 -req -days 9999 -in csr.pem -signkey key.pem -out cert.pem</code>

<code>$ rm csr.pem</code>


<p>OK, that should leave you with a self signed certificate comprised of two files; a key and a cert.</p>

The hard work is done! Let's launch a server. Make sure you are in the /socket_test root directory<br>
<code>$ node server/server.js</code>

<p>The cursor will just blink, that's fine. It's exactly what we want.</p>

<p>When a mobile device connects to your socket.io server, then you will see the data being sent logged to the console.</p>

<p>To access this on a mobile device, you need to be on the same wifi network as the machine hosting the server and also determine the IP addres of that machine.<p>

<p>You can do that by following the steps here:<br>
<code> https://osxdaily.com/2010/11/21/find-ip-address-mac/ </code>
 
Now that you know the IP address, simply visit this URL on your mobile device (again, make sure its on the same wifi network as the host).<br>
<code> https://YOUR_IP_HERE:8443 </code>

For example, if your IP is 1.1.1.1, here's what you input into the mobile browser.<br>
<code> https://1.1.1.1:8443 </code>

<p>You will get security warnings. You need to click "read more" or "accept the risk" or whatever your browser allows, in order to accept the self-signed SSL certificate.</p>

<p>PHEW! Happy coding!</p>
