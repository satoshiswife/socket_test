// connect to the socket
let socket = io();

// data to broadcast/receive
let dataToSend = {
  micVol: "0.0",
  rotX:   "0.0",
  rotY:   "0.0",
  rotZ:   "0.0",
};

// when someone connects, log it to the console
socket.on("connect", () => {
  console.log("connected to server");
});

// when someone disconnects, log it to the console
socket.on("disconnect", () => {
  console.log("disconnected from server");
});

// setup the canvas
function setup() {
  // make a canvase thats the size of the browser window
  let cnv = createCanvas(windowWidth, windowHeight);

  // we have to wait for a user initiated event to get mic access
  cnv.mousePressed(userStartAudio);
  input = new p5.AudioIn();
  input.start();

  // set the framerate, which effectively limits
  // the broadcasts of our socket
  frameRate(30);

  // we add a button to the canvase to grant
  // access to device motion data.
  // especially important to do in iOS 12+
  // adapted from code by @cdaein : https://editor.p5js.org/cdaein/sketches/8i91p06dd
  if (
    typeof DeviceOrientationEvent !== "undefined" &&
    typeof DeviceOrientationEvent.requestPermission === "function"
  ) {
    button = createButton("click to allow access to sensors");
    button.style("font-size", "24px");
    button.center();
    button.mousePressed(requestAccess);
  } else {
    // do nothing, its not a mobile device
  }
  // end citation
}

function draw() {
  // set the background
  background(255);

  // set the values of date to send to the server   
  dataToSend.micVol = input.getLevel();
  dataToSend.rotX = floor(rotationX);
  dataToSend.rotY = floor(rotationY);
  dataToSend.rotZ = floor(rotationZ);

  // send the data on each draw loop,
  // as often as the framerate we set above
  socket.emit("phoneDataSend", dataToSend);
}

// event trigger to allow motion data access
// is granted when button is pushed
// adapted from code by @cdaein : https://editor.p5js.org/cdaein/sketches/8i91p06dd
function requestAccess() {
  DeviceOrientationEvent.requestPermission()
    .then((response) => {
      if (response == "granted") {
        permissionGranted = true;
      }
    })
    .catch(console.error);
  button.remove();
}
// end citation
